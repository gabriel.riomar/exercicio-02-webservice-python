""" doc """
from flask import Flask, render_template


app = Flask(__name__)

@app.route('/')
def pagina_principal():
    return "Página Inicial"





@app.route('/sobre')
def pagina_sobre():
   return render_template('sobre.html')





@app.route('/soma/<int:id1>/<int:id2>')
def soma(id1,id2):
    """
    doc
    """
    operacao = id1 + id2
    contexto = {
            'operacao': operacao,
            'id1': id1,
            'id2': id2
            }
    return render_template('soma.html', contexto=contexto)





@app.route('/contato')
def contato():
    return render_template('contato.html')








if __name__ == '__main__':
    app.run(debug=True)
